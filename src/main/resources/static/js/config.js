app.config(function($routeProvider) {

	$routeProvider.when('/', {
		templateUrl : 'part/home.html',
		controller : 'inicioCtrl'
	}).when('/profesores', {
		templateUrl : 'part/profesores.html',
		controller : 'profesoresCtrl'
	}).when('/alumnos', {
		templateUrl : 'part/alumnos.html',
		controller : 'alumnosCtrl'
	}).when('/alumno/:codigo', {
		templateUrl : 'part/alumno.html',
		controller : 'alumnoCtrl'
	}).otherwise({
		redirectTo : '/'
	});

});