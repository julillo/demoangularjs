package com.jlillo.demo.demoAngularJS2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAngularJs2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoAngularJs2Application.class, args);
	}
}
